<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
		<section>
        	<div class="container">
        		<div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Choose Camera to Review</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="../controller/submitReviewController.jsp" method="POST">
                            <%
                                if(request.getParameter("error")!=null){
                            %>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3 text-center">
                                        <%=request.getParameter("error")%>
                                    </div>
                                       
                                </div>
                            <%
                                }
                            %>
                            <select class="form-control" name="id">
                                <%
                                    String userID = (String)session.getAttribute("userID");
                                    String queryItem = "select * from transaction join item_transaction on transaction.id = transaction_id join item on item_id = item.id where member_id like'"+userID+"' and status like '3'";
                                    ResultSet result = st1.executeQuery(queryItem);
                                    while(result.next()){
                                        int proID = result.getInt("item.id");
                                        String nama = result.getString("productName");
                                %>
                                <option value="<%=proID%>"><%=nama%></option>
                                <%
                                    }
                                %>
                            </select>
                            <textarea class="form-control" name="review">
                                
                            </textarea>
                            <input type="submit" name="submit" class="btn btn-primary">
                        </form>
                    </div>
                </div> 
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>