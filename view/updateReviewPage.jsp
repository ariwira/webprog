<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
		<section>
        	<div class="container">
        		<div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Update Your Review</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="../controller/updateReviewController.jsp" method="POST">
                            <%
                                if(request.getParameter("error")!=null){
                            %>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3 text-center">
                                        <%=request.getParameter("error")%>
                                    </div>
                                       
                                </div>
                            <%
                                }
                            %>
                            <%
                                int revId = Integer.parseInt(request.getParameter("revID"));
                                String querySelect = "select * from review where id like "+revId;
                                ResultSet result = st.executeQuery(querySelect);
                                if (result.next()) {
                                    String content = result.getString("reviewContent");
                                    
                            %>
                            <input type="hidden" name="id" value="<%=revId%>">
                            <input type="textarea" name="review" class="form-control" value="<%=content%>"> 
                            <input type="submit" name="submit" class="btn btn-primary">
                            <%
                                }
                            %>
                        </form>
                    </div>
                </div> 
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>