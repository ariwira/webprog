<!doctype html>
<html>
    <%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/auth.jsp'%>
        <section>
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h2 class="panel-title">Transaction List</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row page-header">
                            <div class="col-md-3">
                                Item List
                            </div>
                            <div class="col-md-2">
                                Total
                            </div>
                            <div class="col-md-2">
                                Status
                            </div>
                            <div class="col-md-2">
                                Date
                            </div>
                            <div class="col-md-2">
                                Buyer
                            </div>
                            <div class="col-md-1">
                                Accept
                            </div>
                        </div>
                        <%
                            String queryItem = "select * from transaction join member on member_id = member.id order by date";
                            ResultSet result = st.executeQuery(queryItem);
                            while(result.next()){
                                int transID = result.getInt("transaction.id");
                                int uID = result.getInt("member_id");
                                String status = result.getString("status");
                                String namaUser = result.getString("fullName");
                                String date = result.getString("date");
                                int total = result.getInt("total");
                        %>
                        <div class="row">
                            <div class="col-md-3">
                                <%
                                    String queryList = "select * from transaction join item_transaction on transaction.id = transaction.id join item on item_id = item.id";
                                    ResultSet rs = st1.executeQuery(queryList);
                                    while(rs.next()){
                                        int qty = rs.getInt("item_transaction.qty");
                                        String gambar = rs.getString("productImage");
                                        String namaProd = rs.getString("productName");
                                %>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <img src="../gambar/<%=gambar%>" style="height: 30px;width: 30px;">
                                    </div>
                                    <div class="col-md-4">
                                        <%=namaProd%>
                                    </div>
                                    <div class="col-md-4">
                                        <%=qty%> pcs
                                    </div>
                                </div>
                                <%
                                    }
                                %>
                            </div>
                            <div class="col-md-2">
                                Rp. <%=total%>
                            </div>
                            <div class="col-md-2">
                                <%if(status.equals("1")){
                                %>
                                    <span class="label label-danger text-center">Waiting for Payment</span>
                                <%}else if(status.equals("2")){%>
                                    <span class="label label-primary text-center">Waiting for Payment Approval</span>
                                <%}else{%>
                                    <span class="label label-success text-center">Order Completed</span>
                                <%
                                    }
                                %>
                            </div>
                            <div class="col-md-2">
                                <%=date%>
                            </div>
                            <div class="col-md-2">
                                <%=namaUser%>
                            </div>
                            <div class="col-md-1">
                                 <%if(status.equals("1")){
                                %>
                                    <span class="glyphicon glyphicon-time"></span>
                                <%}else if(status.equals("2")){%>
                                    <a href="../controller/acceptPaymentController.jsp?trID=<%=transID%>" class="btn btn-success">Accept Payment</a>
                                <%}else{%>
                                    <span class="label label-success">Completed</span>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>  
            </div>
        </section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>