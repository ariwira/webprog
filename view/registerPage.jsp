<!doctype html>
<html>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <section>
        <form class="form-horizontal" action="../controller/registerController.jsp" method="POST">
			<%
				if(request.getParameter("error")!=null){
			%>
				<div class="form-group">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                	<%=request.getParameter("error")%>
	                </div>
	                   
                </div>
			<%
				}
			%>
			<div class="form-group">
		        <label class="col-xs-3 control-label">Full Name :</label>
		        <div class="col-xs-8">
		            <input type="text" name="fullname" class="form-control" placeholder="Full Name">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">UserName:</label>
		        <div class="col-xs-8">
		            <input type="text" name="username" class="form-control" placeholder="UserName">
		            
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Email:</label>
		        <div class="col-xs-8">
		            <input type="email" name="email" class="form-control" placeholder="Email">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Password :</label>
		        <div class="col-xs-8">
		            <input type="password" name="password" class="form-control" placeholder="Password">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Confirm Password :</label>
		        <div class="col-xs-8">
		            <input type="password" name="confirmPassword" class="form-control" placeholder="Confirm Password">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Phone Number :</label>
		        <div class="col-xs-8">
					<input type="text" name="phoneNum" class="form-control" placeholder="Phone Number">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Birth Date :</label>
		        <div class="col-xs-8">
		            <input type="text" name="DoB" class="form-control" placeholder="dd-mm-yyyy">
		        </div>
		    </div>
		  	<div class="form-group">
                <label class="col-xs-3 control-label"></label>
                <div class="col-xs-8">
                    <input type="submit" class="btn btn-primary" value="Save Changes">
                    <span></span>
                    <input type="reset" class="btn btn-default" value="Cancel">
                </div>
            </div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>