<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/auth.jsp'%>
        <%
        	String id = request.getParameter("prodID");
			String query1 = "select * from item where item.id like '"+id+"' ";
			ResultSet rs = st.executeQuery(query1);
			while(rs.next()){
				String gambar = rs.getString("productImage");
				String nama = rs.getString("productName");
				int stock = rs.getInt("stock");
				int harga = rs.getInt("productPrice");
				int berat = rs.getInt("productWeight");
				String spek = request.getParameter("productSpec");
		%>
		<section>
        <form class="form-horizontal" action="../controller/updateProductController.jsp" method="POST">
			<%
				if(request.getParameter("error")!=null){
			%>
				<div class="form-group">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                	<%=request.getParameter("error")%>
	                </div>
	                   
                </div>
			<%
				}
			%>
			<input type="hidden" name="id" value="<%=id%>">
			<div class="form-group">
		        <label class="col-xs-3 control-label">Product Name :</label>
		        <div class="col-xs-8">
		            <input type="text" name="prodName" class="form-control" placeholder="Name" value="<%=nama%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Price:</label>
		        <div class="col-xs-8">
		            <input type="text" name="prodPrice" class="form-control" placeholder="IDR" value="<%=harga%>">
		            
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Weight:</label>
		        <div class="col-xs-8">
		            <input type="text" name="berat" class="form-control" placeholder="grams" value="<%=berat%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Stock :</label>
		        <div class="col-xs-8">
		            <input type="text" name="stock" class="form-control" placeholder="pcs" value="<%=stock%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Specification:</label>
		        <div class="col-xs-8">
		            <input type="text" name="prodSpec" class="form-control" placeholder="Specification" value="<%=spek%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Image:</label>
		        <div class="col-xs-8">
					<input type="file" name="prodImg" class="form-control" placeholder="Image">
		        </div>
		    </div>
		  	<div class="form-group">
                <label class="col-xs-3 control-label"></label>
                <div class="col-xs-8">
                    <input type="submit" class="btn btn-primary" value="Update Product">
                    <span></span>
                    <input type="reset" class="btn btn-default" value="Cancel">
                </div>
            </div>
		</section>
		<%
			}
		%>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>