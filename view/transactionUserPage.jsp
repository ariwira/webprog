<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
		<section>
        	<div class="container">
                <%
                    String userID = (String)session.getAttribute("userID");
                    String query = "select * from transaction where member_id like'"+userID+"' order by date";
                    ResultSet rs = st.executeQuery(query);
                    while(rs.next()){
                        int trID = rs.getInt("transaction.id");
                        String date = rs.getString("date");
                        String status = rs.getString("status");
                        int total = rs.getInt("total");
                %>
        		<div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Transaction ID : <%=trID%></h3>
                        <h3 class="panel-title text-center"><%=date%></h3>
                        <%if(status.equals("1")){
                        %>
                        <a href="../controller/confirmPaymentController.jsp?trID=<%=trID%>" class="btn btn-primary pull-right">Confirm Payment</a>
                        <%
                            }
                        %>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                Image
                            </div>
                            <div class="col-md-3">
                                Name
                            </div>
                            <div class="col-md-3">
                                Quantity
                            </div>
                            <div class="col-md-3">
                                Total Price
                            </div>
                        </div>
                        <%
                            String queryItem = "select * from transaction join item_transaction on transaction.id = transaction_id join item on item_id = item.id where member_id like'"+userID+"' order by date";
                            ResultSet result = st1.executeQuery(queryItem);
                            while(result.next()){
                                int proID = result.getInt("item.id");
                                String gambar = result.getString("productImage");
                                String nama = result.getString("productName");
                                int qty = result.getInt("qty");
                                int subtotal = result.getInt("subtotal");
                        %>
                        <div class="row">
                            <div class="col-md-3">
                                <img src="../gambar/<%=gambar%>" style="height: 60px;width: 60px;">
                            </div>
                            <div class="col-md-3">
                                <%=nama%>
                            </div>
                            <div class="col-md-3">
                                <%=qty%>
                            </div>
                            <div class="col-md-3">
                                Rp. <%=subtotal%>
                            </div>
                        </div>
                        <%
                            }
                        %>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-6">
                                Total
                            </div>
                            <div class="col-md-3">
                                Rp. <%=total%>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="container">
                            <div class="col-md-12">
                                <%if(status.equals("1")){
                                %>
                                    <span class="label label-danger text-center">Waiting for Payment</span>
                                <%}else if(status.equals("2")){%>
                                    <span class="label label-primary text-center">Waiting for Payment Approval</span>
                                <%}else{%>
                                    <span class="label label-success text-center">Order Completed</span>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>    
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>