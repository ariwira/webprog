<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
		<section>
        	<div class="container" style="width: 960px; margin: 0 auto;">
        		<div class="row text-center">
                <%
                    int perPage = 6;
                    int currentPage = 1;
                    String userID = (String)session.getAttribute("userID");
                    if(request.getParameter("page")!=null){
                        currentPage = Integer.parseInt(request.getParameter("page"));
                    }
                    int data = (currentPage-1) * perPage;
                    String query = "select * from item limit "+data+", "+perPage;
                    ResultSet rs = st.executeQuery(query);
                    while(rs.next()){
                        int proID = rs.getInt("id");
                        String nama = rs.getString("productName");
                        int harga = rs.getInt("productPrice");
                        int stock = rs.getInt("stock");
                        String spec = rs.getString("productSpec");
                        String gambar = rs.getString("productImage");
                %>
        			<div class="col-md-4">
        				<div class="row text-center">
                            <img src="../gambar/<%=gambar%>" class="img-rounded" style="height: 50px;width: 50px;">
                        </div>
                        <div class="row text-center">
                            <h3><%=nama%></h3>
                        </div>
                        <div class="row text-center">
                            <%=spec%>
                        </div>
                        <div class="row text-center">
                            <%=stock%>
                        </div>
                        <div class="row text-center">
                            <h3>Rp. <%=harga%></h3>
                        </div>
                        <form class="form-horizontal" action="../controller/addCartController.jsp" method="POST">
                            <input type="hidden" name="prodId" value="<%=proID%>">
                            <input type="hidden" name="userId" value="<%=userID%>">
                            <input type="hidden" name="harga" value="<%=harga%>">
                            <div class="row text-center">
                                <input type="number" name="quantity" class="form-control" min="1" max="<%=stock%>" value="1">
                            </div>
                            <div class="row text-center">
                                <input type="submit" name="submit" class="btn btn-default" value="Add to Cart">
                            </div>
                        </form>
        			</div>
                <%
                    }
                %>	
                </div>
                <nav aria-label="Page navigation text-center">
                    <ul class="pagination">
                    <%
                        String query2 = "select * from item";   
                        rs = st.executeQuery(query2);
                        rs.last();
                        int totalData = rs.getRow();
                        int totalPage = totalData/perPage;
                        if(totalData % perPage!=0){
                            totalPage++;
                        }
                        for(int i = 1 ;i<=totalPage;i++){
                            String url = "?page="+i;
                    %>
                        <li><a href="productListPage.jsp<%=url%>"><%= i%></a></li>
                    <%
                        }
                    %>
                    </ul>
                </nav>
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>