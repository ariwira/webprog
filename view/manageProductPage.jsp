<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/auth.jsp'%>
		<section>
        	<div class="container">
        		<div class="row">
        			<div class="col-md-3">
        				Image
        			</div>
        			<div class="col-md-3">
        				Name
        			</div>
        			<div class="col-md-3">
        				Stock
        			</div>
        			<div class="col-md-3">
        				Manage
        			</div>
        		</div>
        		<%
					String query = "select * from item";
					ResultSet rs = st.executeQuery(query);
					while(rs.next()){
						int proID = rs.getInt("id");
						String gambar = rs.getString("productImage");
						String nama = rs.getString("productName");
						int stock = rs.getInt("stock");
				%>
        		<div class="row">
        			<div class="col-md-3">
        				<img src="../gambar/<%=gambar%>" style="height: 60px;width: 60px;">
        			</div>
        			<div class="col-md-3">
        				<%=nama%>
        			</div>
        			<div class="col-md-3">
        				<%=stock%>
        			</div>
        			<div class="col-md-3">
        				<a href="updateProductPage.jsp?prodID=<%=proID%>" class="btn btn-success">Update</a>
        				<a href="../controller/deleteProductController.jsp?prodID=<%=proID%>" class="btn btn-danger">Delete</a>
        			</div>
        		</div>
        		<%
					}
				%>
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>