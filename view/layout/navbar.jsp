<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button class="toggle navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <img src="img/logo.png" class="navbar-brand">
      <a href="" class="navbar-brand">
        Apperature
      </a>
      <a class="navbar-left navbar-brand">
        <%  if (session.getAttribute("userID") == null) { 
        %>
          Hi Guest
        <%
          }else{
        %>
          Hi <%=session.getAttribute("fullName")%>
        <%
          }
        %>
      </a>
      <a class="navbar-left navbar-brand">
        User Online : 
        <%
          if(application.getAttribute("onlineUser") == null){
        %> 0
        <%}else{%>
          <%= application.getAttribute("onlineUser")%>
          <%
          }
          %>
      </a>
    </div>
    <div class="navbar-collapse collapse" aria-expanded="false">
      <ul class="nav navbar-nav navbar-right">
      <%  if (session.getAttribute("userID") != null) { 
            if (session.getAttribute("roles").equals("2")){
      %>
        <li><a href="/webprog/index.jsp" title="">Home</a></li>
        <li><a href="/webprog/view/transactionUserPage.jsp" title="">Transaction</a></li>
        <li><a href="/webprog/view/cartPage.jsp" title="">Cart</a></li>
        <li><a href="/webprog/view/myReviewPage.jsp" title="">My Review</a></li>
        <li><a href="/webprog/view/allReviewPage.jsp" title="">All Review</a></li>
        <li><a href="/webprog/view/manageAccountPage.jsp" title="">Manage Account</a></li>
        <li><a href="/webprog/view/productListPage.jsp" title="">Product</a></li>
        <li><a href="/webprog/controller/logoutController.jsp" title="">Logout</a></li>
        <%
            }else if(session.getAttribute("roles").equals("1")){
        %>
            <li><a href="/webprog/index.jsp" title="">Home</a></li>
            <li><a href="/webprog/view/insertProduct.jsp" title="">Insert Product</a></li>
            <li><a href="/webprog/view/manageProductPage.jsp" title="">Manage Product</a></li>
            <li><a href="/webprog/view/manageTransactionPage.jsp" title="">Manage Transaction</a></li>
            <li><a href="/webprog/controller/logoutController.jsp" title="">Logout</a></li>
          <%}
          }else{
        %>
        <li><a href="/webprog/index.jsp" title="">Home</a></li>
        <li><a href="view/registerPage.jsp" title="">Register</a></li>
          <form class="navbar-form navbar-left" action="controller/loginController.jsp" method="POST">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Username" name="username">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" placeholder="Password" name="password">
              <%
                if(request.getParameter("error") != null){
              %>
                <span class="help-block">
                    <strong><%=request.getParameter("error")%></strong>
                </span>
              <%}%>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" class="form-control" value="remember" name="remember">
                Remember Me
              </label>
            </div>
            <button type="submit" class="btn btn-default">Login</button>
          </form>
        <%
          }
        %>
      </ul>
    </div>
  </div>
</nav>