<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
		<section>
        	<div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">My Cart</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                Image
                            </div>
                            <div class="col-md-3">
                                Name
                            </div>
                            <div class="col-md-3">
                                Quantity
                            </div>
                            <div class="col-md-3">
                                Total Price
                            </div>
                        </div>
                        <%
                            String userID = (String)session.getAttribute("userID");
                            String query = "select * from cart join item on item_id = item.id where member_id like'"+userID+"'";
                            int totalPrice = 0;int totalQty = 0;
                            ResultSet rs = st.executeQuery(query);
                            while(rs.next()){
                                int proID = rs.getInt("item.id");
                                String gambar = rs.getString("productImage");
                                String nama = rs.getString("productName");
                                int qty = rs.getInt("qty");
                                int subtotal = rs.getInt("subtotal");
                                totalPrice += subtotal;
                                totalQty += qty;
                        %>
                        <div class="row">
                            <div class="col-md-3">
                                <img src="../gambar/<%=gambar%>" style="height: 60px;width: 60px;">
                            </div>
                            <div class="col-md-3">
                                <%=nama%>
                            </div>
                            <div class="col-md-3">
                                <%=qty%>
                            </div>
                            <div class="col-md-3">
                                Rp. <%=subtotal%>
                            </div>
                        </div>
                        <%
                            }
                        %>
                        <div class="row">
                            <a href="../controller/checkoutController.jsp" class="btn btn-success pull-right">Checkout</a>
                            <a href="../controller/deleteCartController.jsp" class="btn btn-danger pull-right">Clear Cart</a>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="container">
                            <div class="col-md-12">
                                <span class="label label-primary pull-right">Total Payment: Rp. <%=totalPrice%></span>
                                <span class="label label-success pull-right">Total Quantity: <%=totalQty%></span>
                            </div>
                        </div>
                    </div>
                </div>    
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>