<!doctype html>
<html>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/auth.jsp'%>
		<section>
        <form class="form-horizontal" action="../controller/insertProductController.jsp" method="POST">
			<%
				if(request.getParameter("error")!=null){
			%>
				<div class="form-group">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                	<%=request.getParameter("error")%>
	                </div>
	                   
                </div>
			<%
				}
			%>
			<div class="form-group">
		        <label class="col-xs-3 control-label">Product Name :</label>
		        <div class="col-xs-8">
		            <input type="text" name="prodName" class="form-control" placeholder="Name">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Price:</label>
		        <div class="col-xs-8">
		            <input type="text" name="prodPrice" class="form-control" placeholder="IDR">
		            
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Weight:</label>
		        <div class="col-xs-8">
		            <input type="text" name="berat" class="form-control" placeholder="grams">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Stock :</label>
		        <div class="col-xs-8">
		            <input type="text" name="stock" class="form-control" placeholder="pcs">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Specification:</label>
		        <div class="col-xs-8">
		            <input type="text" name="prodSpec" class="form-control" placeholder="Specification">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Product Image:</label>
		        <div class="col-xs-8">
					<input type="file" name="prodImg" class="form-control" placeholder="Image">
		        </div>
		    </div>
		  	<div class="form-group">
                <label class="col-xs-3 control-label"></label>
                <div class="col-xs-8">
                    <input type="submit" class="btn btn-primary" value="Save Changes">
                    <span></span>
                    <input type="reset" class="btn btn-default" value="Cancel">
                </div>
            </div>
		</section>

        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>