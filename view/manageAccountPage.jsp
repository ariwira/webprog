<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
        <section>
        <form class="form-horizontal" action="../controller/updateMemberController.jsp" method="POST">
			<%
				if(request.getParameter("error")!=null){
			%>
				<div class="form-group">
	                <div class="col-md-6 col-md-offset-3 text-center">
	                	<%=request.getParameter("error")%>
	                </div>
	                   
                </div>
			<%
				}
				String userID = (String)session.getAttribute("userID");
                String querySelect = "select * from member where member.id like'"+userID+"'";
                ResultSet rs = st.executeQuery(querySelect);
                if(rs.next()){
                	String fullname = rs.getString("fullName");
                	String username = rs.getString("username");
                	String email = rs.getString("email");
                	String phone = rs.getString("phoneNumber");
                	String pass = rs.getString("password");

		%>
			<div class="form-group">
		        <label class="col-xs-3 control-label">Full Name :</label>
		        <div class="col-xs-8">
		            <input type="text" name="fullname" class="form-control" placeholder="Full Name" value="<%=fullname%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">UserName:</label>
		        <div class="col-xs-8">
		            <input type="text" name="username" class="form-control" placeholder="UserName" value="<%=username%>">
		            
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Email:</label>
		        <div class="col-xs-8">
		            <input type="email" name="email" class="form-control" placeholder="Email" value="<%=email%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Password :</label>
		        <div class="col-xs-8">
		            <input type="password" name="password" class="form-control" placeholder="Password" value="<%=pass%>">
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="col-xs-3 control-label">Phone Number :</label>
		        <div class="col-xs-8">
					<input type="text" name="phoneNum" class="form-control" placeholder="Phone Number" value="<%=phone%>">
		        </div>
		    </div>
		  	<div class="form-group">
                <label class="col-xs-3 control-label"></label>
                <div class="col-xs-8">
                    <input type="submit" class="btn btn-primary" value="Save Changes">
                    <span></span>
                    <input type="reset" class="btn btn-default" value="Cancel">
                </div>
            </div>
            <%
				}
            %>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>