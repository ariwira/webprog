<!doctype html>
<html>
	<%@include file="../connector/connector.jsp"%>
    <%@include file='layout/head.jsp'%>
    <body>
        <%@include file='layout/navbar.jsp'%>
        <%@include file='../middleware/authUser.jsp'%>
		<section>
        	<div class="container">
        		 <div class="row page-header">
                    <div class="col-md-4">
                        Image
                    </div>
                    <div class="col-md-4">
                        Name
                    </div>
                    <div class="col-md-4">
                        Review
                    </div>
                 </div>
                 <%
                    int product = 0;
                    String query = "select * from item";
                    ResultSet rs = st.executeQuery(query);
                    while(rs.next()){
                        int proID = rs.getInt("id");
                        String gambar = rs.getString("productImage");
                        String nama = rs.getString("productName");
                %>
                <div class="row">
                    <div class="col-md-4">
                        <img src="../gambar/<%=gambar%>" style="height: 60px;width: 60px;">
                    </div>
                    <div class="col-md-4">
                        <%=nama%>
                    </div>
                    <div class="col-md-4">
                        <a href="allReviewPage.jsp?prodID=<%=proID%>" class="btn btn-success">Show Review</a>
                    </div>
                </div>
                <%
                        if(request.getParameter("prodID")!=null){
                            if(Integer.parseInt(request.getParameter("prodID"))==proID){
                                product = Integer.parseInt(request.getParameter("prodID"));
                                String querySelect = "select * from review join member on member_id = member.id where item_id like "+product;
                                ResultSet result = st1.executeQuery(querySelect);
                                while(result.next()){
                                    int revID = result.getInt("review.id");
                                    int uId = result.getInt("member_id");
                                    int prodId = result.getInt("item_id");
                                    String content = result.getString("reviewContent");
                                    String namaUser = result.getString("fullName");
                %>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">All Review</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <%=namaUser%>
                            </div>
                            <div class="col-md-4">
                                <%=content%>
                            </div>
                            <div class="col-md-4">
                                <a href="updateReviewPage.jsp?revID=<%=revID%>" class="btn btn-primary">Update</a>
                                <a href="../controller/deleteReviewController.jsp?revID=<%=revID%>" class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </div>
                <%
                              }
                %>
                <%
                            }
                %>
                <%
                        }
                %>
                <%
                    }
                %>
        	</div>
		</section>
        <%@include file='layout/footer.jsp'%>
        <%@include file='layout/javascript.jsp'%>
    </body>
</html>