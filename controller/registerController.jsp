<%@ page import="java.io.*" %>
<%@include file="../connector/connector.jsp"%>
<%
	String fname = request.getParameter("fullname");
	String uname = request.getParameter("username");
	String email = request.getParameter("email");
	String pword = request.getParameter("password");
	String confirmPword = request.getParameter("confirmPassword");
	String phone = request.getParameter("phoneNum");
	String lahirnya = request.getParameter("DoB");
	boolean hasError = false;
	String errorMes = "../view/registerPage.jsp?error=";
	//String regexEmail = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	String regexEmail = "(.*)@(.*).(.*)";


	if(fname.isEmpty() || uname.isEmpty() || email.isEmpty() || pword.isEmpty() 
		|| confirmPword.isEmpty() || phone.isEmpty() || lahirnya.isEmpty()){
		errorMes+="Harus diisi<br>";
		hasError = true;
	}
	if (fname.length() < 4 || fname.length() > 20) {
		errorMes+="fullname harus 4-20 character<br>";
		hasError = true;
	}
	if (uname.length() < 5 || uname.length() > 15) {
		errorMes+="username harus 5-15 character<br>";
		hasError = true;
	}
	if (!email.matches(regexEmail)) {
		errorMes+="email harus valid<br>";
		hasError = true;
	}
	if (pword.length() < 7 || pword.length() > 15) {
		errorMes+="password harus 7-15 character<br>";
		hasError = true;	
	}
	if (!confirmPword.equals(pword)) {
		errorMes+="password tidak sama<br>";
		hasError = true;	
	}
	if (phone.length() < 11 || phone.length() > 13) {
		errorMes+="nomor handphone harus valid<br>";
		hasError = true;
	}
	// if (!lahirnya.isNumeric()) {
	// 	errorMes+="Birth Date harus valid dd-mm-yyyy";
	// 	hasError = true;
	// }

	if(hasError){
		response.sendRedirect(errorMes);
	}else{
		String queryRegis = "INSERT INTO member(fullName,username,email,password,phoneNumber,birthdate,roles) VALUES ('"+fname+"','"+uname+"','"+email+"','"+pword+"','"+phone+"','"+lahirnya+"', '2')";
		st.executeUpdate(queryRegis);
		response.sendRedirect("../index.jsp");
	}

%>