<%@ page import="java.io.*,java.util.*" %>
<%@include file="../connector/connector.jsp"%>
<%
   int online = 0;
   String uname = request.getParameter("username");
   String pword = request.getParameter("password");
   String remember = request.getParameter("remember");
   String queryLogin = "select * from member where username='"+uname+"'and password='"+pword+"'";
   ResultSet result = st.executeQuery(queryLogin);
   if (result.next()) {
      String userID = result.getString("id");
      String fullName = result.getString("fullName");
      String roles = result.getString("roles");
      session.setAttribute("fullName",fullName);
      session.setAttribute("userID",userID);
      session.setAttribute("roles",roles);
      if(application.getAttribute("onlineUser")!=null){
         online = (Integer)application.getAttribute("onlineUser");
         online++;
         application.setAttribute("onlineUser",online);
      }else{
         online = 0;
         online++;
         application.setAttribute("onlineUser",online);
      }

      if(remember != null){
         Cookie cookie_uname = new Cookie("username",uname);
         Cookie cookie_pword = new Cookie("password",pword);
         cookie_uname.setMaxAge(3600*24);
         cookie_uname.setPath("/");
         cookie_pword.setMaxAge(3600*24);
         cookie_pword.setPath("/");
         response.addCookie(cookie_uname);
         response.addCookie(cookie_pword);
      }

      if(roles.equals("2")){
         response.sendRedirect("../index.jsp");
      }else if(roles.equals("1")){
         response.sendRedirect("../index.jsp");
      }
   }else{
      response.sendRedirect("../index.jsp?error=invalid");
   }
%>