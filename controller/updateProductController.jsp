<%@ page import="java.io.*,java.util.*" %>
<%@include file="../connector/connector.jsp"%>

<%
	String id = request.getParameter("id");
	String name = request.getParameter("prodName");
	String harga = request.getParameter("prodPrice");
	String berat = request.getParameter("berat");
	String stock = request.getParameter("stock");
	String spek = request.getParameter("prodSpec");
	String gambar = request.getParameter("prodImg");
	boolean hasError = false;
	String errorMes = "../view/insertProduct.jsp?error=";
	// int hargaInt = Integer.parseInt(harga);
	// int beratInt = Integer.parseInt(berat);
	// int stockInt = Integer.parseInt(stock);
	String regexNumeric = "^[0-9]{1,45}$";
	String regexGambar =  "([^\\s]+(\\.(?i)(jpg|png|jpeg))$)";

	if(name.isEmpty() || harga.isEmpty() || berat.isEmpty() || stock.isEmpty() 
		|| spek.isEmpty() || gambar.isEmpty()){
		errorMes+="Harus diisi<br>";
		hasError = true;
		response.sendRedirect(errorMes);
	}else{
		if (!harga.matches(regexNumeric) || !berat.matches(regexNumeric) ||!stock.matches(regexNumeric) ) {
			errorMes+="harus valid berupa numbers<br>";
			hasError = true;
			response.sendRedirect(errorMes);
		}else{
			int hargaInt = Integer.parseInt(harga);
			int beratInt = Integer.parseInt(berat);
			int stockInt = Integer.parseInt(stock);
			if(hargaInt < 1000000 || hargaInt > 50000000){
				errorMes+="harga harus antara sejuta - 50juta <br>";
				hasError = true;
			}
			if (stockInt < 1) {
				errorMes+="stock harus lebih dari 0<br>";
				hasError = true;
			}
			if (!gambar.matches(regexGambar)) {
				errorMes+="format gambar harus valid (jpg|png|jpeg) <br>";
				hasError = true;
			}
			
			if (hasError) {
				response.sendRedirect(errorMes);
			}else {
				String query = "UPDATE item SET productName = '"+name+"',productPrice="+hargaInt+",productWeight="+beratInt+",stock="+stockInt+",productSpec='"+spek+"',productImage='"+gambar+"' WHERE id='"+id+"'";
				st.executeUpdate(query);
				response.sendRedirect("../index.jsp");
			}
		}
	}
%>