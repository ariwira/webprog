<%@ page import="java.io.*" %>
<%@include file="../connector/connector.jsp"%>
<%
	String userID = (String)session.getAttribute("userID");
	String fname = request.getParameter("fullname");
	String uname = request.getParameter("username");
	String email = request.getParameter("email");
	String pword = request.getParameter("password");
	String phone = request.getParameter("phoneNum");
	boolean hasError = false;
	String errorMes = "../view/manageAccountPage.jsp?error=";
	//String regexEmail = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	String regexEmail = "(.*)@(.*).(.*)";
	String regexNumeric = "^[0-9]{1,45}$";
	String regexAlphabet = "^[A-z]+$";
	String regexAlphanum = "^[a-zA-Z0-9]*$";


	if(fname.isEmpty() || uname.isEmpty() || email.isEmpty() || pword.isEmpty() || phone.isEmpty()){
		errorMes+="Harus diisi<br>";
		hasError = true;
		response.sendRedirect(errorMes);
	}else{
		if (fname.length() < 4 || fname.length() > 20) {
			errorMes+="fullname harus 4-20 character<br>";
			hasError = true;
			response.sendRedirect(errorMes);
		}else if(!fname.matches(regexAlphabet)){
			errorMes+="fullname harus alphabet<br>";
			hasError = true;
			response.sendRedirect(errorMes);
		}else{
			if (uname.length() < 5 || uname.length() > 15) {
				errorMes+="username harus 5-15 character<br>";
				hasError = true;
				response.sendRedirect(errorMes);
			}else if(!uname.matches(regexAlphanum)){
				errorMes+="username harus alphanumeric<br>";
				hasError = true;
				response.sendRedirect(errorMes);
			}else{
				if (!email.matches(regexEmail)) {
					errorMes+="email harus valid<br>";
					hasError = true;
					response.sendRedirect(errorMes);
				}else{
					if (pword.length() < 7 || pword.length() > 15) {
						errorMes+="password harus 7-15 character<br>";
						hasError = true;	
						response.sendRedirect(errorMes);
					}else{
						if (phone.length() < 11 || phone.length() > 13) {
							errorMes+="nomor handphone harus 11-13 digit<br>";
							hasError = true;
							response.sendRedirect(errorMes);
						}else if(!phone.matches(regexNumeric)){
							errorMes+="nomor handphone harus valid<br>";
							hasError = true;
							response.sendRedirect(errorMes);
						}else{
							String query = "UPDATE member SET fullName = '"+fname+"',username="+uname+",email='"+email+"',password='"+pword+"',phoneNumber='"+phone+"' WHERE id='"+userID+"'";
							st.executeUpdate(query);
							response.sendRedirect("../index.jsp");
						}
					}
				}
			}
		}
	}

%>